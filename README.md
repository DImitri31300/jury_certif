# **GUIDE D'UTILISATION DU PROJET: "Jury"**

Tout d'abord, pour la bonne utilisation de ce projet, voici le contexte du projet : 

Lors du passage d'un titre professionnel, les jurés sont amenés à se prononcer sur l'octroi d'un titre professionnel, sur la base :

- soit de l’examen du candidat en train de réaliser son métier en situation réelle.
- soit de la présentation différée, par le candidat, d'une situation d'exercice de sa profession, lorsque cette activité ne peut être réalisée sur un temps assez court pour être jugé.

**Durant ces évaluations, de nombreux candidats peuvent être examinés, et pour chacun d'eux, un nombre d'activités et compétences doivent être validées, ainsi que des compétences transverses. 
Créons pour les jurés une interface simple pour leur simplifier les prises de notes pour chacun des candidats, ainsi que les compétences et activités validées, 
ou nécessitant clarification durant l'entretien technique.**

## Gestion de projet :

Pour assurer la bonne répartition de nos tâches et le suivi en temps réel de notre avancement dans ce projet, nous avons utilisé l'outil Trello: 

[Trello](https://trello.com/b/BrOO2lrw/gestion-de-projet)

Pour que notre site puisse être réalisable, avant même d'avoir commencé le projet, nous avons réalisé des maquettes, le site correspond donc à chaque maquette pour une meilleur clarté de la vision du projet.

## Contenu du projet : 

- *Architecture* :

Ce projet est un projet développé en Python à l'aide du framework Django, la base de données est stockée sur Firebase.

- *Contenu* :

Premièrement, en tant que jury, vous pourrez vous identifier sur la plateforme de connexion prévue à cet effet en arrivant sur le site.
Nous avons directement pris la liberté d'ajouter vos adresses mails à notre projet pour qu'aucun problème de connexion se produise.

Ensuite, vous aurez la possibilité d'ajouter un candidat pour l'évaluer ainsi que de voir la liste de ces candidats.
Ces candidats auront chacun un prénom, un nom et leur modalité d'examen à renseigner.

Une fois que votre candidat sera créé, vous pourrez voir spécifiquement la fiche de chaque candidat avec ses données renseignées ainsi que chaque compétences à évaluer.
Pour assurer une bonne évaluation de chaque compétences et favoriser la prise de note, chaque compétence pourra contenir des remarques associées à une compétence ciblée.

Enfin, quand votre remarque sera remplie, vous n'aurez plus qu'à soumettre la remarque et le tableau des compétences se mettra à jour avec les remarques que vous aurez remplies.

**MERCI !**

BONUS: si tout cela est fonctionnel, nous implanterons en plus un tableau de bord administrateur et nous vous assignerons en tant que Super utilisateur pour gérer les flux de données existants dans notre application.
