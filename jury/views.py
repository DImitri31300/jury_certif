from django.shortcuts import render 
from django.shortcuts import redirect 
import pyrebase


# Configuration avec Firebase
config = {
    'apiKey': "AIzaSyD3erckZC_NnlYTMnOj59-yHkEo80h17UQ",
    'authDomain': "jury-ac9b2.firebaseapp.com",
    'databaseURL': "https://jury-ac9b2.firebaseio.com",
    'projectId': "jury-ac9b2",
    'storageBucket': "jury-ac9b2.appspot.com",
    'messagingSenderId': "118160704002",
    'appId': "1:118160704002:web:12ab49e2b113654af09d1b"
}
# Initialisation de l'application 
firebase = pyrebase.initialize_app(config)
auth = firebase.auth()

# Fonction signIn()
def singIn(request):
    return render(request, "signIn.html")

# Fonction postsign()
def postsign(request):
    email=request.POST.get('email')
    passw = request.POST.get("pass")

    try:
        user = auth.sign_in_with_email_and_password(email,passw)
    except:
        message = "invalid crediantials"  
        return render(request, "signIn.html", {"msg":message})

    # print(user)
    return render(request, "welcome.html",{"e":email})

#Connexion database
db = firebase.database()

# Fonction de redirection vers la vue 'Créer une tâche'
def create(request):
    
    return render(request, "create.html")

# Fonction d'envoi de données, quand on créer une tâche
def post_create(request):
    
    name=request.POST.get('name')
    nickname=request.POST.get('nickname')
    modalite=request.POST.get('modalite')

    data = {
        "name":name,
        "nickname":nickname,
        "modalite":modalite
    }
    user= db.child("users").push(data)



    return redirect('/liste/')

# Fonction de redirection vers la vue 'Liste des tâches'
def liste(request):
    users=db.child("users").get()
    data = users
    listData = []
    for users in data.each():
        key=users.key()
        utilisateur=users.val()
        result= {
            "clé":key,
            "nom":utilisateur['name'],
            "nom_famille":utilisateur['nickname'],
            "modalite":utilisateur['modalite']
        }
        # print(result)
        listData.append(result)
        
    # print(listData)
    return render(request, "liste.html", {'data': listData})


# def profil(request, key_id):
#     users=db.child("users").get()
#     data = users
#     for users in data.each():
#         key_id=users.key()
#     return render(request, "profil.html", {'profil': key_id})


def profil(request, key_id):
    users=db.child("users").get()
    data = users
    listData = []
    for users in data.each():
        if users.key() == key_id:
            print('toto')
        key=users.key()
        utilisateur=users.val()
        result= {
            "clé":key,
            "nom":utilisateur['name'],
            "nom_famille":utilisateur['nickname'],
            "modalite":utilisateur['modalite']
        }
        # print(result)
        listData.append(result)
    return (request, "profil.html", key_id) 
